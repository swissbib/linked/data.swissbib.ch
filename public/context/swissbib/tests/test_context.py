from rdflib import Graph, plugin
from rdflib.serializer import Serializer
import json


if __name__ == '__main__':

    with open('../document.json') as docs:
        documents_context = json.load(docs)

    with open('../bibliographicResource.json') as docs:
        resources_context = json.load(docs)

    with open('../person.json') as docs:
        persons_context = json.load(docs)

    with open('../organisation.json') as docs:
        organisations_context = json.load(docs)

    with open('../item.json') as docs:
        items_context = json.load(docs)

    resource_graph = Graph().parse(location='resource.test.json', format='json-ld', context=resources_context)
    with open('resource.out.ttl', 'w') as fp:
        fp.write(resource_graph.serialize(format='turtle').decode())

    item_graph = Graph().parse(location='item.test.json', format='json-ld', context=items_context)
    with open('item.out.ttl', 'w') as fp:
        fp.write(item_graph.serialize(format='turtle').decode())

    document_graph = Graph().parse(location='document.test.json', format='json-ld', context=documents_context)
    with open('document.out.ttl', 'w') as fp:
        fp.write(document_graph.serialize(format='turtle').decode())

    organisation_graph = Graph().parse(location='organisation.test.json', format='json-ld', context=organisations_context)
    with open('organisation.out.ttl', 'w') as fp:
        fp.write(organisation_graph.serialize(format='turtle').decode())

    sb_organisation_graph = Graph().parse(location='sb-organisation.test.json', format='json-ld', context=organisations_context)
    with open('sb-organisation.out.ttl', 'w') as fp:
        fp.write(sb_organisation_graph.serialize(format='turtle').decode())