import sbt._

object Dependencies {
  lazy val scalaTestPlusPlugin = "org.scalatestplus.play" %% "scalatestplus-play" % "5.1.0"
  lazy val elasticsearchRestHighLevelClient = "org.elasticsearch.client" % "elasticsearch-rest-high-level-client" % "7.6.2"
  lazy val jacksonDatabind = "com.fasterxml.jackson.core" % "jackson-databind" % "2.11.4"
  lazy val log4jCore = "org.apache.logging.log4j" % "log4j-core" % "2.16.0"
}
