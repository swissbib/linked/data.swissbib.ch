addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.8.11")
addSbtPlugin("org.foundweekends.giter8" % "sbt-giter8-scaffold" % "0.12.0")
addSbtPlugin("io.github.givesocialmovement" % "sbt-vuefy" % "6.0.0")
addSbtPlugin("com.typesafe.sbt" % "sbt-git" % "1.0.0")
