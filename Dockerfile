FROM docker.io/hseeberger/scala-sbt:11.0.8_1.4.1_2.13.3 as build
RUN apt update -y --allow-releaseinfo-change
RUN curl -sL https://deb.nodesource.com/setup_16.x | bash -
RUN apt install -y nodejs
RUN npm install --global yarn
WORKDIR /build
ADD . ./
RUN yarnpkg install && sbt clean stage

FROM docker.io/amd64/adoptopenjdk:11-jre
COPY --from=build /build/target/universal/stage /app/
WORKDIR /app
ENTRYPOINT ./bin/swissbib-rest


