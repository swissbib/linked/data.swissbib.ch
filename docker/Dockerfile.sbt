ROM hseeberger/scala-sbt:8u181_2.12.7_1.2.6 AS build
# Preliminary build
WORKDIR /build
RUN sbt new scala/scala-seed.g8 --name=build -o . && rm -r src/test
COPY build.sbt .
COPY project/ ./project/
RUN sbt compile

# Copy and build actual source code
ADD src/ ./src
RUN sbt assembly

# Build deployable Docker image
FROM openjdk:8-jre-alpine
ADD src/main/resources/config.properties /app/config/config.properties
COPY --from=build /build/target/scala-2.12/app.jar /app/app.jar
CMD java -jar /app/app.jar