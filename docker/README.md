# Docker Image Template

This template can directly be included in a project:

```sh
# .gitlab-ci.yml

include:
  - project: 'memoriav/memobase-2020/utilities/ci-templates'
    file: 'docker/default.yml'

# (...)
```

Additionally there are some example Dockerfiles. They normally consist of a
two-tier build. The first one is responsible for building the code, while the
second one includes this build in a deployable and preferably very small
Docker image. Adapt the Dockerfiles to your needs.
