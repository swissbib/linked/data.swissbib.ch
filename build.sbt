import Dependencies._

ThisBuild / scalaVersion := "2.13.7"
ThisBuild / organizationName := "swissbib"
ThisBuild / organization := "org.swissbib"
ThisBuild / git.gitTagToVersionNumber := { tag: String =>
  if (tag matches "[0-9]+\\..*") Some(tag)
  else None
}

ThisBuild / PlayKeys.devSettings.withRank(KeyRanks.Invisible) := Seq("play.server.http.port" -> "9003")

lazy val root = (project in file("."))
  .enablePlugins(PlayScala, SbtWeb, SbtVuefy) // Enable the plugin
  .settings(
    name := "swissbib-rest",
    libraryDependencies ++= Seq(
      elasticsearchRestHighLevelClient,
      guice,
      jacksonDatabind,
      log4jCore,
      ws,
      scalaTestPlusPlugin % Test
    ),
    Assets / VueKeys.vuefy / VueKeys.prodCommands := Set("stage"),
    Assets / VueKeys.vuefy / VueKeys.webpackBinary := "./node_modules/.bin/webpack",
    Assets / VueKeys.vuefy / VueKeys.webpackConfig := "./webpack.config.js"
  )
