# data.swissbib.ch

<https://data.swissbib.ch> provides a Rest API to the content of <https://www.swissbib.ch> in a Linked Open Data Format.

See the [documentation](https://data.swissbib.ch/documentation).

The application uses [Play Framework (Scala)](https://www.playframework.com/) and Vuejs for rendering (via this [plugin](https://give.engineering/2018/06/05/vue-js-with-playframework.html)). 

## Setup

Setting your own development configuration :

```sh
cd conf
cp development.conf.dist development.conf
```
Change the `template_prefix` to your own name to avoid conflicts with other developers. Adapt the ports if you prefer. 

Then : tunnel to ES7 server :

```sh
ssh -L 8080:sb-ues5.swissbib.unibas.ch:8080 swissbib@sb-ues5.swissbib.unibas.ch
```

First-time setup : 

```sh
yarn install
sbt run
```

Then :

```sh
sbt run
```

is enough.


## Setup in Kubernetes via Helm Charts
Install
```sh
helm install data-swissbib-prod-ch ./helm-charts --namespace=ub-digitale-dienste
```

Uninstall
```sh
helm uninstall data-swissbib-prod-ch --namespace=ub-digitale-dienste
```

A secret is needed, you can create it with
```sh
kubectl create secret generic data-swissbib-ch-application-secret --namespace=ub-digitale-dienste --from-literal=secret=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
```
and replace XXXXXXXXXXXXXXXXXXXXXX with a valid string of your choice for a play application secret cf. https://www.playframework.com/documentation/2.8.x/ApplicationSecret
