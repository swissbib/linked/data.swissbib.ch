package libraries

object Format extends Enumeration {
  type Format = Value

  val HTML: Value = Value("html")
  val JSON_LD: Value = Value("json")
  val JSON_LINES: Value = Value("jsonl")
}
