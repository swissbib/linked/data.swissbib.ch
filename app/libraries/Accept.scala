package libraries

import play.api.http.MediaRange

object Accept {

  private val formatMap = Map(
    Format.HTML -> Tuple2("html", Set("text/html")),
    Format.JSON_LD -> Tuple2("json", Set("application/json", "application/ld+json")),
    Format.JSON_LINES -> Tuple2("jsonl", Set("application/x-jsonlines"))
  )

  def getFormat(format: String, acceptHeaders: Seq[MediaRange]): Option[Format.Value] = {
    val acceptHeaderFormat = checkAcceptHeaders(acceptHeaders)
    val formatFormat = checkFormat(format)
    formatFormat match {
      case Some(value) => Some(value)
      case None =>
        acceptHeaderFormat match {
          case Some(value) => Some(value)
          case None => None
        }
    }
  }

  def formatOrHtml(format: String, acceptHeaders: Seq[MediaRange]): Format.Value = {
    getFormat(format, acceptHeaders) match {
      case Some(value) => value
      case None =>Format.HTML
    }
  }

  private def checkAcceptHeaders(acceptHeaders: Seq[MediaRange]): Option[Format.Value] = {
    val acceptHeadersStrings = acceptHeaders.map(value => value.toString())
    formatMap.find(value => (value._2._2 intersect acceptHeadersStrings.toSet).nonEmpty) match {
      case Some(value) => Some(value._1)
      case None => None
    }
  }

  private def checkFormat(format: String): Option[Format.Value] = {
    formatMap.find(value => value._2._1 == format) match {
      case Some(value) => Some(value._1)
      case None => None
    }
  }
}