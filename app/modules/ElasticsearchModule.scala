package modules

import com.google.inject.AbstractModule

class ElasticsearchModule extends AbstractModule{

  override def configure(): Unit = {
    bind(classOf[ElasticsearchComponent])
      .to(classOf[ElasticsearchClient])
      .asEagerSingleton()
  }
}
