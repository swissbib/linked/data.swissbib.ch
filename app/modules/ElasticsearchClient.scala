package modules

import java.io.IOException

import com.typesafe.config.Config
import javax.inject.{Inject, Singleton}
import org.apache.http.message.BasicHeader
import org.apache.http.{Header, HttpHost}
import org.elasticsearch.action.admin.cluster.storedscripts.PutStoredScriptRequest
import org.elasticsearch.client.{RequestOptions, RestClient, RestHighLevelClient}
import org.elasticsearch.common.bytes.BytesReference
import org.elasticsearch.common.xcontent.{XContentFactory, XContentType}
import play.Environment
import play.api.inject.ApplicationLifecycle
import utilities.FileUtil

import scala.collection.mutable.ArrayBuffer
import scala.concurrent.Future

@Singleton
class ElasticsearchClient @Inject()(
                              lifecycle: ApplicationLifecycle,
                              private val config: Config,
                              private val env: Environment
                            ) extends ElasticsearchComponent {
  lifecycle.addStopHook(() => {
    Future.successful(client.close())
  })

  val client: RestHighLevelClient = connect()

  uploadTemplates()

  private def connect(): RestHighLevelClient = {
    val hosts = new ArrayBuffer[HttpHost]
    config.getStringList("index.hosts").forEach(
      value => {
        val hostPort = value.split(":")
        hosts += new HttpHost(hostPort(0), hostPort(1).toInt)
      }
    )
    val headers = Array(new BasicHeader("cluster.name", config.getString("index.cluster")).asInstanceOf[Header])
    new RestHighLevelClient(RestClient.builder(hosts.toArray : _*).setDefaultHeaders(headers))
  }

  private def uploadTemplates(): Unit = {
    config.getStringList("index.templatequeries").forEach((templateName: String) => {
        val template = FileUtil.readFile(templateName, env)
        val nameWithoutPath = templateName.substring(templateName.lastIndexOf("/") + 1).replaceAll(".mustache", "")
        val templatePrefix = config.getString("index.template_prefix")
        try {
          val request = new PutStoredScriptRequest
          request.id(templatePrefix + "_" + nameWithoutPath)
          val builder = XContentFactory.jsonBuilder
          builder.startObject
          builder.startObject("script")
          builder.field("lang", "mustache")
          // load mustache source as string to allow invalid JSON template features!
          builder.field("source", template.replace("\n", ""))
          builder.endObject
          builder.endObject
          request.content(BytesReference.bytes(builder), XContentType.JSON)
          client.putScript(request, RequestOptions.DEFAULT)
        } catch {
          case io: IOException =>
            //TODO: if the templates cannot be read the application should shut down.
            io.printStackTrace()
        }
    })
  }
}
