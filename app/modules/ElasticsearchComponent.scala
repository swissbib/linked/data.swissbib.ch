package modules

import org.elasticsearch.client.RestHighLevelClient

trait ElasticsearchComponent {
  val client: RestHighLevelClient
}
