package controllers

import akka.http.scaladsl.model.StatusCodes.ClientError
import akka.stream.scaladsl
import akka.util.ByteString
import javax.inject._
import libraries.{Accept, Format}
import modules.ElasticsearchComponent
import org.elasticsearch.action.search.{ClearScrollRequest, SearchResponse, SearchScrollRequest}
import org.elasticsearch.client.RequestOptions
import org.elasticsearch.common.unit.TimeValue
import play.api.Configuration
import play.api.libs.json.{JsArray, JsObject, JsValue, Json}
import play.api.mvc._
import utilities._
/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class ConceptController @Inject()(cc: ControllerComponents,
                                  config: Configuration,
                                  es: ElasticsearchComponent) extends AbstractController(cc) with Rendering {
  val searchHelper = new SearchHelper(es, config)
  def byId(id: String, format: String, concept: String): Action[AnyContent] = Action { implicit request =>
    val responseFormat = Accept.formatOrHtml(format, request.acceptedTypes)
    try {
      val response = searchHelper.searchById(id, ConceptType.withName(concept))
      responseFormat match {
        case libraries.Format.HTML => Ok(views.html.details(response.toString, concept))
        case libraries.Format.JSON_LD => Ok(response.toString).as("application/ld+json")
        case libraries.Format.JSON_LINES => Ok(response.toString).as("application/ld+json")
      }
    } catch {
      case e: Throwable => NotFound(views.html.error(e.getMessage, concept))
    }
  }

  def search(concept: String, q: String, exists: String, format: String, page: Int, size: Int): Action[AnyContent] = Action { implicit request =>
    val responseFormat = Accept.formatOrHtml(format, request.acceptedTypes)
    try {
      val conceptType = ConceptType.withName(concept)
      responseFormat match {
        case libraries.Format.HTML =>
          val response = searchHelper.search(q, exists, responseFormat, page, size, conceptType)
          Ok(views.html.resultList(response, concept, q))
        case libraries.Format.JSON_LD =>
          val response = searchHelper.search(q, exists, responseFormat, page, size, conceptType)
          Ok(response.toString).as("application/ld+json")
        case libraries.Format.JSON_LINES =>
          val response = initiateScroll(q, exists, concept)
          executeStreamResponse(response, concept)
      }
    } catch {
      case e: Throwable => BadRequest(views.html.error(e.getMessage, concept))
    }
  }

  private def scrollIterator(searchResponse: SearchResponse): Iterator[ByteString] = {
    new Iterator[ByteString]() {
      private var hits = searchResponse.getHits.iterator()
      private var scrollId = searchResponse.getScrollId

      override def hasNext: Boolean = {
        if (!hits.hasNext) {
          val response = scrollRequest(scrollId)
          hits = response.getHits.iterator()
          scrollId = response.getScrollId
          if (!hits.hasNext) {
            val clearScrollRequest = new ClearScrollRequest()
            clearScrollRequest.addScrollId(scrollId)
            es.client.clearScroll(clearScrollRequest, RequestOptions.DEFAULT)
            false
          } else {
            true
          }
        } else {
          true
        }
      }

      override def next(): ByteString = {
        ByteString.fromString(hits.next().getSourceAsString + "\n")
      }
    }
  }

  private def initiateScroll(q: String, exists: String, concept: String): SearchResponse = {
    val templateSearchRequest = new SearchTemplateRequestBuilder(config)
    templateSearchRequest.setRequest(concept, scroll = true)
      .setScriptType()
      .setScript(q, exists, concept)
      .setScriptParams(q: String, exists, 1, 200)
    es.client.searchTemplate(templateSearchRequest.searchTemplateRequest, RequestOptions.DEFAULT).getResponse
  }

  private def scrollRequest(scrollId: String): SearchResponse = {
    val request = new SearchScrollRequest(scrollId)
    request.scroll(TimeValue.timeValueMinutes(10L))
    es.client.scroll(request, RequestOptions.DEFAULT)
  }

  def advancedSearch(concept: String, q: String, exists: String, format: String, page: Int = 1, size: Int = 10): Action[AnyContent] = Action { implicit request =>
    val responseFormat = Accept.formatOrHtml(format, request.acceptedTypes)
    try {
      val advancedSearchHelper = new AdvancedSearchHelper(q, page, size, ConceptType.withName(concept), config, es.client)
      responseFormat match {
        case libraries.Format.HTML =>
          val hydraCollection = advancedSearchWithPaging(advancedSearchHelper, q, exists, responseFormat, page, size, concept)
          Ok(views.html.resultList(hydraCollection, concept, ""))
        case libraries.Format.JSON_LD =>
          val hydraCollection = advancedSearchWithPaging(advancedSearchHelper, q, exists, responseFormat, page, size, concept)
          Ok(hydraCollection).as("application/ld+json")
        case libraries.Format.JSON_LINES =>
          val response = advancedSearchHelper.execute(true)
          executeStreamResponse(response, concept)
      }
    } catch {
      case e: Throwable => BadRequest(views.html.error(e.getMessage, concept))
    }
  }

  def advancedSearchWithPaging(
                                advancedSearchHelper: AdvancedSearchHelper,
                                q: String,
                                exists: String,
                                responseFormat: Format.Value,
                                page: Int,
                                size: Int,
                                concept: String): JsValue = {
    val response = advancedSearchHelper.execute()
    val pagingHelper = new PagingMetadataHelper(q, exists, responseFormat, page, size, response.getHits.getTotalHits.value, ConceptType.withName(concept), config)
    val hydraPartialCollectionView = pagingHelper.advancedSearchPaging()
    val hits: IndexedSeq[JsValue] = response.getHits.getHits.map(value => Json.parse(value.getSourceAsString)).toIndexedSeq
    Json.obj(
      "@id" -> hydraPartialCollectionView("@id"),
      "@type" -> "hydra:Collection",
      "@context" -> Json.obj(
        "hydra" -> "http://www.w3.org/ns/hydra/core#"
      ),
      "hydra:member" -> new JsArray(hits),
      "hydra:totalItems" -> response.getHits.getTotalHits.value,
      "hydra:view" -> hydraPartialCollectionView
    )
  }

  def executeStreamResponse(response: SearchResponse, concept: String): Result = {
    val source = scaladsl.Source.fromIterator(() => scrollIterator(response))
    Ok.chunked(source)
      .withHeaders(("Content-Disposition", s"attachment; filename=swissbib-${concept}-bulk.jsonl"))
      .as("application/x-jsonlines")
  }


  def propertyMap(concept: String): Action[AnyContent] = Action { implicit request =>
    val response = PropertyHelper.getPropertyMap(ConceptType.withName(concept), config, es)
    Ok(response.toString).as("application/json")
  }

  def propertyList(concept: String): Action[AnyContent] = Action { implicit request =>
    val response = PropertyHelper.getPropertyList(ConceptType.withName(concept), config, es)
    Ok(response.toString).as("application/json")
  }
}
