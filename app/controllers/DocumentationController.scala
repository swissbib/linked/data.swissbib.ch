package controllers
import play.api.Configuration
import play.api.mvc.{AbstractController, Action, ControllerComponents}
import javax.inject._



/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
class DocumentationController @Inject()(cc: ControllerComponents,
                                        config: Configuration
                                       ) extends AbstractController (cc) {


  def documentation() = Action { implicit request =>

    Ok(views.html.documentation())
  }
}
