package utilities

import libraries.Format
import play.api.Configuration
import play.api.libs.json.{JsValue, Json}

class PagingMetadataHelper(
                            q: String,
                            exists: String,
                            format: Format.Value,
                            pageParam: Int,
                            sizeParam: Int,
                            totalItems: Long,
                            concept: ConceptType.Value,
                            configuration: Configuration)
{

  private val size = sizeParam
  private val lastPage = ((totalItems + size - 1) / size).toInt
  private val page = if (pageParam > lastPage) lastPage else pageParam

  private val baseUrl = configuration.get[String]("baseUrl")

  def advancedSearchPaging(): JsValue = {
    var value = Json.obj(
      "@id" -> advancedSearchUrl(),
      "hydra:first" -> advancedSearchUrl(1),
      "hydra:last" -> advancedSearchUrl(lastPage),
      if (page == lastPage) "hydra:limit" -> totalItems % size else "hydra:limit" -> size
    )
    if (page > 1) value =  value ++ Json.obj("hydra:previous" -> advancedSearchUrl(page - 1))
    if (page < lastPage) value = (value ++ Json.obj("hydra:next" -> advancedSearchUrl(page + 1)))
    value
  }

  def searchPaging(): JsValue = {
    var value = Json.obj(
      "@id" -> searchUrl(),
      "hydra:first" -> searchUrl(1),
      "hydra:last" -> searchUrl(lastPage),
      if (page == lastPage) "hydra:limit" -> totalItems % size else "hydra:limit" -> size
    )
    if (page > 1) value =  value ++ Json.obj("hydra:previous" -> searchUrl(page - 1))
    if (page < lastPage) value = (value ++ Json.obj("hydra:next" -> searchUrl(page + 1)))
    value
  }

  private def advancedSearchUrl(pageValue: Int = page): String = baseUrl +
    controllers.routes.ConceptController.advancedSearch(concept, q, exists, format.toString, pageValue, size)

  private def searchUrl(pageValue: Int = page): String = baseUrl + controllers.routes.ConceptController.search(
    concept = concept, q = q, exists = exists, format = format.toString, page = pageValue, size)
}
