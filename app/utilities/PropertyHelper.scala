package utilities

import modules.ElasticsearchComponent
import org.elasticsearch.client.RequestOptions
import org.elasticsearch.client.indices.GetMappingsRequest
import play.api.Configuration
import play.api.libs.json.{JsValue, Json}

import scala.collection.mutable

object PropertyHelper {
  def getPropertyMap(concept: ConceptType.Value, configuration: Configuration, es: ElasticsearchComponent): JsValue = {
    val source = getPropertiesMap(configuration.get[String](s"concept2index.$concept"), es)
    val properties = mutable.HashMap[String, List[String]]()
    source.keySet().stream().forEach {
      value =>  {
        val values = value.split(":")
        // prevent properties without namespaces to find their way into the list (for example @id, @type & @context.
        if (values.size == 2) {
          val ns = values(0)
          if (properties.contains(ns)) {
            properties(ns) = values(1) :: properties(ns)
          } else {
            properties(ns) = List(values(1))
          }
        }
      }
    }
    if (properties.contains("rdf")) {
      properties("rdf") = "type" :: properties("rdf")
    } else {
      properties("rdf") = List("type")
    }
    Json.toJson(properties)
  }


  def getPropertyList(concept: ConceptType.Value, configuration: Configuration, es: ElasticsearchComponent): JsValue = {
    val source = getPropertiesMap(configuration.get[String](s"concept2index.$concept"), es)

    val properties = mutable.Set[String]()
    source.keySet().stream().forEach {
      value =>  {
        val values = value.split(":")
        // prevent properties without namespaces to find their way into the list (for example @id, @type & @context.
        if (values.size == 2) {
          val qName = values(1)
          properties.add(s"${expand(values(0), configuration)}$qName")
        }
      }
    }
    // add @type as special case property... will need special handling...
    properties += "http://www.w3.org/1999/02/22-rdf-syntax-ns#type"
    Json.toJson(properties)
  }


  private def expand(prefix: String, configuration: Configuration): String = {
    configuration.get[String](s"namespaces.$prefix")
  }


  private def getPropertiesMap(index: String, es: ElasticsearchComponent): java.util.HashMap[String, Object] = {
    val request = new GetMappingsRequest()
    request.indices(index)
    val result = es.client.indices().getMapping(request, RequestOptions.DEFAULT)

    val mappingData = result.mappings().values().iterator().next()
    val source = mappingData.getSourceAsMap.get("properties").asInstanceOf[java.util.HashMap[String, Object]]
    source
  }

}
