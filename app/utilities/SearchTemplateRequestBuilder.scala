package utilities

import java.util.regex.Pattern

import org.elasticsearch.action.search.SearchRequest
import org.elasticsearch.common.unit.TimeValue
import org.elasticsearch.script.ScriptType
import org.elasticsearch.script.mustache.SearchTemplateRequest
import play.api.Configuration

import scala.jdk.CollectionConverters._

class SearchTemplateRequestBuilder(val configuration: Configuration) {
  val searchTemplateRequest = new SearchTemplateRequest()
  val templatePrefix: String = configuration.get[String]("index.template_prefix") + "_"

  private val matchAllQuery = Pattern.compile("^$|\\*:\\*|\\*")

  def setRequest(concept: String, scroll: Boolean): SearchTemplateRequestBuilder = {
    val request = new SearchRequest(configuration.get[String]("concept2index." + concept))
    if (scroll) {
      request.scroll(TimeValue.timeValueMinutes(1L))
    }
    searchTemplateRequest.setRequest(request)
    this
  }

  def setScriptType(): SearchTemplateRequestBuilder = {
    searchTemplateRequest.setScriptType(ScriptType.STORED)
    this
  }

  def setScript(q: String, exists: String, concept: String): SearchTemplateRequestBuilder = {
    if (matchAllQuery.matcher(q).find()) {
      if (exists.isEmpty) {
        if (concept == "person" || concept == "organisation") {
          searchTemplateRequest.setScript(templatePrefix + template("matchAllThumbnailFirstQuery"))
        } else {
          searchTemplateRequest.setScript(templatePrefix + template("matchAllQuery"))
        }
      } else {
        searchTemplateRequest.setScript(templatePrefix + template("existsFilterQuery"))
      }
    } else {
      if (exists.isEmpty) {
        searchTemplateRequest.setScript(templatePrefix + template("multiMatchQuery"))
      } else {
        searchTemplateRequest.setScript(templatePrefix + template("existsFilterQuery"))
      }
    }
    this
  }

  private def template(value: String): String = {
    configuration.get[String]("query2template." + value)
  }

  def setScriptParams(q: String, exists: String, page: Int, size: Int): SearchTemplateRequestBuilder = {
    val builder = new ScriptParamBuilder()
      .setSizeAndPage(page, configuration, size)

    if (exists.isEmpty) {
      builder.setSimpleParamQuery(q)
    } else {
      builder.setExistsFilterQuery(q, exists)
    }
    searchTemplateRequest.setScriptParams(builder.map.asJava)
    this
  }

  def setSimulate(): SearchTemplateRequestBuilder = {
    searchTemplateRequest.setSimulate(true)
    this
  }
}



