package utilities

import java.util
import java.util.regex.Pattern

import play.api.Configuration

import scala.jdk.CollectionConverters._

class ScriptParamBuilder {
  var map: Map[String, AnyRef] = Map()
  private val matchAllQuery = Pattern.compile("^$|\\*:\\*|\\*")

  def setSizeAndPage(page: Int, configuration: Configuration, sizeParam: Int): ScriptParamBuilder = {
    val size = if (sizeParam > 0 && sizeParam <= configuration.get[Int]("index.maxsize")) sizeParam else configuration.get[Int]("index.defaultsize")
    map = map + ("size" -> size.asInstanceOf[AnyRef])
    map = map + ("from" -> ((page - 1)*size).asInstanceOf[AnyRef])
    this
  }

  def setSimpleParamQuery(q: String): ScriptParamBuilder = {
    if (!matchAllQuery.matcher(q).find()) {
      map = map + ("param_query" -> q)
    }
    this
  }

  def setExistsFilterQuery(q: String, exists: String): ScriptParamBuilder = {
    val list = new util.ArrayList[util.Map[String, util.Map[String, String]]]()
    if (matchAllQuery.matcher(q).find()) {
      list.add(Map("match_all" -> Map[String, String]().asJava).asJava)
    } else {
      list.add(Map("multi_match" -> Map("query" -> q).asJava).asJava)
    }
    if (!exists.isEmpty) {
      val properties = exists.split(',')
      // TODO: Add a check if a property even exists?
      for (property <- properties) {
        list.add(Map("exists" -> Map("field" -> property).asJava).asJava)
      }
      map = map + ("filters" -> list)
    }
    this
  }


}
