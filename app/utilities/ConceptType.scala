package utilities

object ConceptType extends Enumeration {

  protected case class Val(concept: String) extends super.Val
  import scala.language.implicitConversions
  implicit def getConcept(x: Value): String = x.asInstanceOf[Val].concept

  val bibliographicResource: Val = Val("bibliographicResource")
  val work: Val = Val("work")
  val person: Val = Val("person")
  val organisation: Val = Val("organisation")
  val item: Val = Val("item")
  val document: Val = Val("document")

}
