package utilities

import libraries.Format
import modules.ElasticsearchComponent
import org.elasticsearch.action.get.{GetRequest, GetResponse}
import org.elasticsearch.client.RequestOptions
import org.elasticsearch.script.mustache.{SearchTemplateRequest, SearchTemplateResponse}
import play.api.Configuration
import play.api.libs.json.{JsArray, JsValue, Json}

class SearchHelper(
                    val es: ElasticsearchComponent,
                    val configuration: Configuration) {

  def search(q: String,
             exists: String,
             format: Format.Value,
             page: Int,
             size: Int,
             concept: ConceptType.Value
            ): JsValue = {
    if (page<1) {
      throw new Exception("Page number should be positive");
    }
    if (size<1) {
      throw new Exception("Number of results requested should be positive");
    }
    if (size>configuration.get[Int]("index.maxsize")) {
      throw new Exception("Cannot request more than "+configuration.get[Int]("index.maxsize")+" results in one request. But you can get dumps. See documentation.");
    }
    if (size*page>10000) {
      throw new Exception("Cannot go further than the 10000th result");
    }
    val sTB : SearchTemplateRequest = createTemplateRequest(q, page, exists, concept, size)
    val searchTemplateResponse: SearchTemplateResponse = es.client.searchTemplate(sTB, RequestOptions.DEFAULT)
    val totalHits = searchTemplateResponse.getResponse.getHits.getTotalHits.value
    val pagingMetadataHelper = new PagingMetadataHelper(q, exists, format, page, size, totalHits
      , concept, configuration)
    val hydraPartialCollectionView = pagingMetadataHelper.searchPaging()
    val hits: IndexedSeq[JsValue] = searchTemplateResponse.getResponse.getHits.getHits.map(value => Json.parse(value.getSourceAsString)).toIndexedSeq
    Json.obj(
      "@id" -> hydraPartialCollectionView("@id"),
      "@type" -> "hydra:Collection",
      "@context" -> Json.obj(
        "hydra" -> "http://www.w3.org/ns/hydra/core#"
      ),
      "hydra:member" -> new JsArray(hits),
      "hydra:totalItems" -> totalHits,
      "hydra:view" -> hydraPartialCollectionView
    )
  }

  def searchById(id: String,
                 concept: ConceptType.Value): JsValue = {
    val getRequest = new GetRequest(configuration.get[String](s"concept2index.${concept}"), id)
    val getResponse = es.client.get(getRequest, RequestOptions.DEFAULT)
    if (getResponse.isExists) {
      toJsValue(getResponse)
    } else {
      throw new Exception(concept.toString+" with id "+id+" does not exist")
    }
  }

  private def toJsValue (response: GetResponse): JsValue = {
    val sourceMap = response.getSourceAsMap
    Json.parse(play.libs.Json.toJson(sourceMap).toString)
  }

  private def createTemplateRequest(q: String,
                                    page: Int,
                                    exists: String,
                                    concept: ConceptType.Value,
                                    size: Int): SearchTemplateRequest = {
    val builder = new SearchTemplateRequestBuilder(configuration)
      .setRequest(concept, scroll = false)
      .setScriptType()
      .setScript(q, exists, concept)
      .setScriptParams(q, exists, page, size)
      // for testing...
      //.setSimulate()
    builder.searchTemplateRequest
  }


}