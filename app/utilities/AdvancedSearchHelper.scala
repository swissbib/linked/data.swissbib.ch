package utilities

import org.elasticsearch.action.search.{SearchRequest, SearchResponse}
import org.elasticsearch.client.{RequestOptions, RestHighLevelClient}
import org.elasticsearch.common.unit.TimeValue
import org.elasticsearch.index.query.{Operator, QueryBuilders}
import org.elasticsearch.search.builder.SearchSourceBuilder
import org.elasticsearch.search.sort.SortOrder
import play.api.Configuration

class AdvancedSearchHelper( private val query: String,
                            private val page: Int,
                            private val resultListSize: Int,
                            private val concept: ConceptType.Value,
                            private val configuration: Configuration,
                            private val elastic: RestHighLevelClient) {
  if (page<1) {
    throw new Exception("Page number should be positive");
  }
  if (resultListSize<1) {
    throw new Exception("Number of results requested should be positive");
  }
  if (resultListSize>configuration.get[Int]("index.maxsize")) {
    throw new Exception("Cannot request more than "+configuration.get[Int]("index.maxsize")+" results in one request. But you can get dumps. See documentation.");
  }
  if (resultListSize*page>10000) {
    throw new Exception("Cannot go further than the 10000th result");
  }

  private def cleanQuery(): String = {
    var resultQuery = query
    val namespaces = configuration.get[Map[String, String]]("namespaces").keySet
    for (ns <- namespaces) {
      resultQuery = resultQuery.replace(ns + ":", ns + "\\:")
    }
    resultQuery = resultQuery.replace("http:", "http\\:")
    resultQuery = resultQuery.replace("https:", "https\\:")
    if (resultQuery.count(c => c == '/') != 2) {
      resultQuery = resultQuery.replace("/", "\\/")
    }
    resultQuery
  }

  def execute(scroll: Boolean = false): SearchResponse = {
    val request = buildRequest(scroll)
    elastic.search(request, RequestOptions.DEFAULT)
  }

  private def buildRequest(scroll: Boolean): SearchRequest = {
    val queryBuilder = QueryBuilders
      .queryStringQuery(cleanQuery())
      .defaultOperator(Operator.AND)
      .escape(false)

    val searchSourceBuilder = new SearchSourceBuilder()
      .query(queryBuilder)
      .from(page * resultListSize - resultListSize)
      .size(resultListSize)
      .trackTotalHits(true)

    concept match {
      case utilities.ConceptType.bibliographicResource =>
        queryBuilder.defaultField("dct:title")
      case utilities.ConceptType.work =>
      case utilities.ConceptType.person =>
        queryBuilder.defaultField("rdfs:label")
        searchSourceBuilder.sort("dbo:thumbnail", SortOrder.DESC)
      case utilities.ConceptType.organisation =>
        queryBuilder.defaultField("rdfs:label")
        searchSourceBuilder.sort("dbo:thumbnail", SortOrder.DESC)
      case utilities.ConceptType.item =>
        queryBuilder.defaultField("bibo:locator")
      case utilities.ConceptType.document =>
        queryBuilder.defaultField("bf:local")
    }



    val request = new SearchRequest(index)
    request.source(searchSourceBuilder)
    if (scroll) request.scroll(TimeValue.timeValueMinutes(5L))
    request
  }

  private def index: String = configuration.get[String]("concept2index." + concept)
}
