package utilities

import java.io.IOException
import java.nio.file.{Files, Paths}

import play.{Environment, Logger}

object FileUtil {

  def readFile(name: String, env: Environment): String = try {
    val file = env.getFile(name)
    val path = Paths.get(file.getAbsolutePath)
    String.join("\n", Files.readAllLines(path))
  } catch {
    case e: IOException =>
      Logger.error("Could not load mustache template: " + name, e)
      ""
  }
}
